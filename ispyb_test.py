# -*- coding: utf-8 -*-
from requests import Session
from requests.auth import HTTPBasicAuth
from zeep import Client
from zeep.transports import Transport
import json

host = "https://ispyb.esrf.fr/ispyb/ispyb-ws/ispybWS/"
user = "opd29"
password = "*****"
beamline = "bm29"

proposal = ("opd", "29")

session = Session()
session.auth = HTTPBasicAuth(user, password)

_shipping_client = Client(
    host + "ToolsForShippingWebService?wsdl",
    transport=Transport(session=session),
)

_collection_client = Client(
    host + "ToolsForCollectionWebService?wsdl",
    transport=Transport(session=session),
)

_biosaxs_client = Client(
    host + "ToolsForBiosaxsWebService?wsdl",
    transport=Transport(session=session),
)

_generic_biosaxs_client = Client(
    host + "GenericSampleChangerBiosaxsWebService?wsdl",
    transport=Transport(session=session),
)

def find_sessions(code, number):
    return _collection_client.service.findSessionsByProposalAndBeamLine(
        code + number, beamline
    )

def find_proposal(code, number):
    return _shipping_client.service.findProposal(code, number)

def get_test_samples():
    data = {
        "samples": [
            {
            "name": "water",
            "concentration": 1.0,
            "energy": 12.5,
            "plate": 2,
            "recuperation": False,
            "volume": 50.0,
            "id": "",
            "viscosity": 'low',
            "column": 11,
            "buffer_mode": "Before",
            "comment": "water calibration",
            "transmission": 100.0,
            "row": "D",
            "seu_temperature": 4.0,
            "exposure_time": 1.0,
            "flow": True,
            "storage_temperature": 4.0,
            "buffer_name": "bwater",
            "wait": 0.0,
            "num_frames": 7,
            "extra_flow_time": 5.0,
            },
        ],
        "buffers": [
            {
            "name": "bwater",
            "concentration": 0.0,
            "energy": 12.5,
            "plate": 2,
            "row": "D",
            "column": 4,
            "recuperation": False,
            "volume": 10.0,
            "id": "",
            "viscosity": 'low',
            "comment": "",
            "transmission": 100.0,
            "seu_temperature": 20.0,
            "exposure_time": 1.0,
            "flow": False,
            "storage_temperature": 4.0,
            "wait": 0.0,
            "num_frames": 7,
            "extra_flow_time": 5.0,
            },
        ],
    }
    return data


def store_sc_experiment(name, samples, buffers):
    # import pdb; pdb.set_trace()
    pcode, pnumber = "opd", "29" 
    storage_temperature = samples[0]['storage_temperature']
    buffer_mode = samples[0]['buffer_mode']
    extra_flow_time = samples[0]['extra_flow_time']
    experiment_name = name

    samples = [{
        "plate": sample['plate'],
        "well": sample['column'],
        "row": sample['row'],
        "enable": True,
        "macromolecule": '',
        "title": '',
        "transmission": sample['transmission'],
        "recuperate": sample['recuperation'],
        "SEUtemperature": sample['seu_temperature'],
        "viscosity": sample['viscosity'],
        "code": "",
        "typen": "",
        "waittime": 0.0,
        "concentration": sample['concentration'],
        "type": "Sample"
    } for sample in samples]

    buffers = [{
        "plate": buffer['plate'],
        "well": buffer['column'],
        "row": buffer['row'],
        "enable": True,
        "macromolecule": '',
        "title": '',
        "transmission": buffer['transmission'],
        "recuperate": buffer['recuperation'],
        "SEUtemperature": buffer['seu_temperature'],
        "viscosity": buffer['viscosity'],
        "code": "",
        "typen": "",
        "waittime": 0.0,
        "concentration": buffer['concentration'],
        "type": "Sample"
    } for buffer in buffers]

    _biosaxs_client.service.createExperiment(
        pcode,
        pnumber,
        json.dumps(samples + buffers),
        storage_temperature,
        buffer_mode,
        extra_flow_time,
        "TEMPLATE",
        "",
        experiment_name)
    # import pdb; pdb.set_trace()


# on IPYTHON we run it as bellow 
# store_sc_experiment('Store_Test', get_test_samples()['samples'], get_test_samples()['buffers'])  


def list_experiments():
    pcode, pnumber = "opd", "29"
    return _biosaxs_client.service.findExperimentByProposalCode(pcode, pnumber)


